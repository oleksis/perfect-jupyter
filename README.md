# Perfect

![Van Halen-Hot_For_Teacher-Google-Colab-Screenshot 2024-03-03 083733](https://github.com/oleksis/perfect-jupyter/assets/44526468/c0879b5d-2774-4b2c-9df3-6c47d96874b9)

Is about loving **Free and open-source software** ([FOSS](https://en.wikipedia.org/wiki/Free_and_open-source_software)) Projects like [Python](https://www.python.org/) 💖🐍

[![Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/github/oleksis/perfect-jupyter/blob/main/Perfect.ipynb)

## Screenshots

![Perfect Jupyter Notebook](docs/perfect-jupyter-google-colab.png)

## Motivation

You dont need download a media (audio/video) just fork the repo, play with Jupyter Notebook using @code and share a song with a Pull Request

We can create a Playlist from the Community using GitHub

Happy coding! 💖🐍

## Share

- Twitter / X

```txt
🎧 Check out this amazing song: 🐍 https://youtu.be/V09fr_IaYis 

#ShareTheSong
```

## Playlists

Youtube

1. [🎵](https://www.youtube.com/playlist?list=PLKOe5c5VIUYVpgRImJCNZt_qETNqDIyZ4)
2. [🎼]()
